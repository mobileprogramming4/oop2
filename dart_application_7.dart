abstract class Vahicle {
  void goAhead() {
    print("Straight ahead");
  }

  void turnLeft() {
    print("To the left");
  }

  void turnRight() {
    print("To the right");
  }

  void stop() {
    print("Stop moving");
  }
}

mixin Flyable {
  void fly() {
    print("I can fly");
  }
}

mixin Speakable {
  void speak() {
    print("I can speak, Hello");
  }
}

mixin PlayMusic {
  void turnOnMusic() {
    print("Turn on the music");
  }
}

class Plane extends Vahicle with Flyable {}

class Car extends Vahicle with PlayMusic {}

class Robot extends Vahicle with Speakable {}

void showPLaneAbility(Plane plane) {
  print("==========I am plane==========");
  plane.fly();
  plane.goAhead();
  plane.turnLeft();
  plane.turnRight();
  plane.stop();
  print("");
}

void showCarAbility(Car car) {
  print("==========I am car==========");
  car.turnOnMusic();
  car.goAhead();
  car.turnLeft();
  car.turnRight();
  car.stop();
  print("");
}

void showRobotAbility(Robot robot) {
  print("==========I am robot==========");
  robot.speak();
  robot.goAhead();
  robot.turnLeft();
  robot.turnRight();
  robot.stop();
  print("");
}

void main(List<String> arguments) {
  Plane plane = Plane();
  Car car = Car();
  Robot robot = Robot();
  showPLaneAbility(plane);
  showCarAbility(car);
  showRobotAbility(robot);
}
